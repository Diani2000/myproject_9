#include<stdio.h>

int main(){
	
	FILE* filewrite;
	filewrite= fopen("assignment9.txt","w");
	fprintf(filewrite,"UCSC is one of the leading institutes in Sri Lanka for computing studies.");
	fclose(filewrite);
	
	FILE*fileread;
	fileread= fopen("assignment9.txt","r");
	char sentence[50];
	while(!feof(fileread)){
	    fgets(sentence,50,fileread);
	    puts(sentence);
	}
	fclose(fileread);
	
	FILE* fileappend;
	fileappend= fopen("assignment9.txt","a");
	fprintf(fileappend,"\nUCSC offers undergraduate and postgraduate level courses aiming a range of computing fields.");
	fclose(fileappend);
	
	return 0;
}
